function circle=circleMask(maskX, maskY, maskZ, centerX, centerY, centerZ, diameter)

    diameter = diameter + 2;

    [X, Y, Z] = meshgrid(1:maskY, 1:maskX, 1:maskZ);
    
    circle = (X - centerX).^2 + (Y - centerY).^2 + (Z - centerZ).^2 - (diameter/2).^2;
    circle = abs(circle) <= (diameter/2);
    
end
