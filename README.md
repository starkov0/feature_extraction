### Feature Extraction ###

This repository contains feature extraction algorithms for 2D and 3D data in Matlab.

### 2D Feature extraction pipeline ###

Algorithms available for 2D data are :

* 2D data *basic statistical features*
* Histogram
* Histogram *basic statistical features*
* Riesz feature energies
* Moment features
* Local binary pattern *basic statistical features*
* Co-occurrence *basic statistical features*
* Gabor *basic statistical features*
* Wavelet *basic statistical features*

### 3D Feature extraction pipeline ###

Algorithms available for 3D data are :

* 3D data *basic statistical features*
* Histogram
* Histogram *basic statistical features*
* Local binary pattern *basic statistical features*
* Co-occurrence *basic statistical features*
* Wavelet *basic statistical features*

### Basic statistical features ### 

2D and 3D basic statistical features contain :

* min
* max
* median
* mean 
* std
* skewness
* kurtosis
* entropy

### 2D Example ###

An code example may be found in 

```
#!bash

features2D/example2D.m


```

### 2D Lib ###

All code for 2D feature extraction may be found in 

```
#!bash

features2D/lib/


```

### 3D Example ###

An code example may be found in 

```
#!bash

features3D/example3D.m


```

### 3D Lib ###

All code for 3D feature extraction may be found in 

```
#!bash

features3D/lib/


```

### Comments on 2D Riesz features ###

Riesz wavelet filter bank is an implementation of the Steerable Pyramid.(https://en.wikipedia.org/wiki/Pyramid_(image_processing)#Steerable_pyramid)

N and J are Riesz wavelet features parameters.
N corresponds to the steerable pyramid's scale.
J corresponds to the number of filters per scale. 

With a small N (N=[1,3]), you will get high frequency features. 
With a large N (N=[5,7]), you will get high and low frequencies.
When choosing a large N, all scales up to N will be used.
Ex: N=5 corresponds to a concatenation of N=1, N=2, N=3, N=4, N=5.

With a small J (J=[1,3]), you will slightly map your data.
With a large J (J=[5,7]), you will map your data at a very high level.

### SIFT3D ###

SIFT3D code is available for Linux, OS X and Windows machine via :

https://github.com/bbrister/SIFT3D

http://www.mathworks.com/matlabcentral/fileexchange/52800-bbrister-sift3d